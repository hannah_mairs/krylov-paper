
Coherent performance models are important to describe and evaluate algorithms on modern architectures.
With good models, HPC users can pick the appropriate method and algorithmic parameters for their application to run in a complicated computing environment. 
Futhermore, models are important in evaluating HPC systems and moving forward with new architecture and algorithm design.

The increasing complexity of computing environments has introduced many sources of run-to-run variability at different levels of HPC systems. 
For example, an operating system can interrupt processors at any time for its activities, causing detours in computations and 
degrading performance \cite{hoefler2010characterizing, FerreiraBridgesBrightwell08}.
Inter-job contention for shared resources such as network bandwidth, routers, and links is another source of variability that 
can affect application performance \cite{parker2017early, chunduri2017run}. 
Developing coherent performance models in the presence of variability is difficult
\cite{beckman2006influence, HoeflerLumsdaineRehm07}, particularly between systems. 

In this work, we advocate for casting performance models in a nondeterministic  
paradigm that more accurately reflects algorithm execution in noisy HPC environments. 
Employing stochastic performance models for algorithms that run on HPC systems could reflect a more realistic computing 
scenario since many detours and sources of variability are unpredictable.
Some work has been done using stochastic models to predict performance in HPC environments. 
For example, \cite{agarwal2005impact}  studied the impact of different noise distributions on a single collective operation and \cite{seelam2010extreme}
modeled operating system ``jitter" as random variables in order to compute computational slowdown in the presence of noise.

We are interested in modeling standard and pipelined Krylov methods \cite{saad96iterative,VanDerVorst2003} often used in large-scale
simulations to solve sparse systems of equations. 
Highly synchronous applications such as those that use Krylov methods are vulnerable to performance degradation induced 
by variability at different machine levels. 
Because each iteration is performed in lockstep, an operating system interrupt (or any other source of slowdown) that delays 
a single processor can cause others to be delayed as well.
On modern machines, where the cost of communication can vary considerably between iterations and runs due to network 
contention or other factors, this can significantly degrade Krylov method performance because of the global reduction 
operations used to compute vector norms and inner products.
Motivated by mitigating latency associated with global communication, algebraically equivalent pipelined 
versions of Krylov methods have been introduced 
\cite{Chronopoulos_Gear_1989, GhyselsAshbyMeerbergenVanroose2013, GhyselsVanroose2014, StrzodkaGoddeke06, 
Sturler_Vorst_1995, JacquesNicolasVollaire12}. 
Pipelined methods rearrange computations so that it is possible to hide some of the cost of global 
communication with local computation, at the cost of increased total local computation, storage, and numerical stability.

We characterize the performance of standard and pipelined Krylov methods using experimental data to refine the performance 
model introduced in \cite{morgan2016krylov}, which used random variables to model iteration times.
We detail the results of experiments performed at the Argonne Leadership Computing Facility (ALCF) and the Swiss National 
Supercomputing Center. 
Such models and data are useful because pipelined Krylov methods tend to become more useful in situations where performance 
data is more difficult to directly gather: with large node counts on congested supercomputers with unpredictable local processing times.
Data is found to directly support the conjecture that pipelined methods effectively smooth out variability which in and of itself can give speedup over
standard Krylov methods.
This demonstrates that in addition to helping to address the scalability bottleneck associated with global reductions, pipelined Krylov 
methods can, by relaxing data dependencies, offer increased performance even when reductions are modeled as simple barriers; 
this points to these methods being useful in a much wider range of applications, such as those with smaller problem sizes but more 
variable local times per iteration, or on systems with extremely fast networks, currently far away from a reduction latency scaling bottleneck.
We find that our simple and interpretable stochastic performance 
models can accurately describe runtimes and suggest a way to perform a priori estimates.


This paper is organized as follows: Section \ref{sec:model-review} reviews the performance models introduced in \cite{morgan2016krylov} 
for Kylov and pipelined Krylov methods in the presence of noise. In Section \ref{sec:experimental-results} we examine experimental results 
from runs of Krylov and pipelined Krylov methods and refine the performance models in Section \ref{sec:updated-model} based on 
insights from the data. Specifically, we intend to show the need for non-stationary models.
In Section \ref{sec:performance-model} we employ our stochastic performance models and show that the updated model is in close 
agreement with reality.  In Sections \ref{sec:performance-estimates} and \ref{sec:more-experiments} we present more experimental 
data and suggest ways to perform a priori performance estimates.  Finally, we conclude our work in Section \ref{sec:conclusion}. 


